package ru.t1.skasabov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.model.SessionDTO;
import ru.t1.skasabov.tm.dto.model.UserDTO;
import ru.t1.skasabov.tm.enumerated.Role;

public interface IAuthService {

    @NotNull
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    void invalidate(@Nullable SessionDTO session);

    @NotNull
    SessionDTO validateToken(@Nullable String token);

}
