package ru.t1.skasabov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.skasabov.tm.dto.request.*;
import ru.t1.skasabov.tm.dto.response.*;

@NoArgsConstructor
public final class ProjectEndpoint implements IProjectEndpoint {

    @NotNull
    @Override
    public ProjectChangeStatusByIdResponse changeProjectStatusById(
            @NotNull final ProjectChangeStatusByIdRequest request
    ) {
        return IProjectEndpoint.newInstance().changeProjectStatusById(request);
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @NotNull final ProjectChangeStatusByIndexRequest request
    ) {
        return IProjectEndpoint.newInstance().changeProjectStatusByIndex(request);
    }

    @NotNull
    @Override
    public ProjectClearResponse clearProjects(@NotNull final ProjectClearRequest request) {
        return IProjectEndpoint.newInstance().clearProjects(request);
    }

    @NotNull
    @Override
    public ProjectCreateResponse createProject(@NotNull final ProjectCreateRequest request) {
        return IProjectEndpoint.newInstance().createProject(request);
    }

    @NotNull
    @Override
    public ProjectGetByIdResponse getProjectById(@NotNull final ProjectGetByIdRequest request) {
        return IProjectEndpoint.newInstance().getProjectById(request);
    }

    @NotNull
    @Override
    public ProjectGetByIndexResponse getProjectByIndex(@NotNull final ProjectGetByIndexRequest request) {
        return IProjectEndpoint.newInstance().getProjectByIndex(request);
    }

    @NotNull
    @Override
    public ProjectListResponse listProjects(@NotNull final ProjectListRequest request) {
        return IProjectEndpoint.newInstance().listProjects(request);
    }

    @NotNull
    @Override
    public ProjectRemoveByIdResponse removeProjectById(@NotNull final ProjectRemoveByIdRequest request) {
        return IProjectEndpoint.newInstance().removeProjectById(request);
    }

    @NotNull
    @Override
    public ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull final ProjectRemoveByIndexRequest request) {
        return IProjectEndpoint.newInstance().removeProjectByIndex(request);
    }

    @NotNull
    @Override
    public ProjectUpdateByIdResponse updateProjectById(@NotNull final ProjectUpdateByIdRequest request) {
        return IProjectEndpoint.newInstance().updateProjectById(request);
    }

    @NotNull
    @Override
    public ProjectUpdateByIndexResponse updateProjectByIndex(@NotNull final ProjectUpdateByIndexRequest request) {
        return IProjectEndpoint.newInstance().updateProjectByIndex(request);
    }

    @NotNull
    @Override
    public ProjectStartByIdResponse startProjectById(@NotNull final ProjectStartByIdRequest request) {
        return IProjectEndpoint.newInstance().startProjectById(request);
    }

    @NotNull
    @Override
    public ProjectStartByIndexResponse startProjectByIndex(@NotNull final ProjectStartByIndexRequest request) {
        return IProjectEndpoint.newInstance().startProjectByIndex(request);
    }

    @NotNull
    @Override
    public ProjectCompleteByIdResponse completeProjectById(@NotNull final ProjectCompleteByIdRequest request) {
        return IProjectEndpoint.newInstance().completeProjectById(request);
    }

    @NotNull
    @Override
    public ProjectCompleteByIndexResponse completeProjectByIndex(@NotNull final ProjectCompleteByIndexRequest request) {
        return IProjectEndpoint.newInstance().completeProjectByIndex(request);
    }

}
