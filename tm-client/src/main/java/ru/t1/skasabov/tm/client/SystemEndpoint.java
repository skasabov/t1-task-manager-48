package ru.t1.skasabov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.skasabov.tm.dto.request.*;
import ru.t1.skasabov.tm.dto.response.*;

@NoArgsConstructor
public final class SystemEndpoint implements ISystemEndpoint {

    @NotNull
    @Override
    public ApplicationAboutResponse getAbout(@NotNull final ApplicationAboutRequest request) {
        return ISystemEndpoint.newInstance().getAbout(request);
    }

    @NotNull
    @Override
    public ApplicationVersionResponse getVersion(@NotNull final ApplicationVersionRequest request) {
        return ISystemEndpoint.newInstance().getVersion(request);
    }

    @NotNull
    @Override
    public ApplicationSystemInfoResponse getSystemInfo(@NotNull final ApplicationSystemInfoRequest request) {
        return ISystemEndpoint.newInstance().getSystemInfo(request);
    }

    @NotNull
    @Override
    public ApplicationUpdateSchemaResponse updateSchema(@NotNull final ApplicationUpdateSchemaRequest request) {
        return ISystemEndpoint.newInstance().updateSchema(request);
    }

    @NotNull
    @Override
    public ApplicationDropSchemaResponse dropSchema(@NotNull final ApplicationDropSchemaRequest request) {
        return ISystemEndpoint.newInstance().dropSchema(request);
    }

}
