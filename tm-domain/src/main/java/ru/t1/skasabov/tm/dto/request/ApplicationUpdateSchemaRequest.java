package ru.t1.skasabov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ApplicationUpdateSchemaRequest extends AbstractRequest {

    @Nullable
    private String password;

    public ApplicationUpdateSchemaRequest(@Nullable final String password) {
        this.password = password;
    }

}
